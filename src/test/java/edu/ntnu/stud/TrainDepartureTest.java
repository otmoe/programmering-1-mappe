package edu.ntnu.stud;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class TrainDepartureTest {

  @Nested
  @DisplayName("Positive tests for the TrainDeparture class")
  public class PositiveTrainDepartureTest {

    @Test
    @DisplayName("TrainDeparture constructor does not throw exp on valid values")
    public void TrainDepartureConstructorDoesNotThrowException() {
      try {
        TrainDeparture trainDeparture =
            new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
        assertNotNull(trainDeparture);
      } catch (IllegalArgumentException e) {
        fail("The test failed, because the constructor did throw a exceptio: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("setTrack does not throw exp on valid values")
    void setTrackNotThrowException() {
      TrainDeparture trainDeparture =
          new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
      int expResult = 15;
      trainDeparture.setTrack(15);
      int result = trainDeparture.getTrack();
      assertEquals(expResult, result);
    }
  }

  @Nested
  @DisplayName("Negative tests for the TrainDeparture class")
  public class NegativeTrainDepartureTest {

    @Test
    @DisplayName("TrainDeparture constructor does throw exp on non valid line value")
    public void TrainDepartureConstructorDoesNotThrowExceptionLine() {
      try {
        TrainDeparture trainDeparture =
            new TrainDeparture(LocalTime.of(0, 10), "", 15, "Bergen", 12, LocalTime.of(0, 5));
        fail("The test failed, because the constructor did not throw the exception");
      } catch (IllegalArgumentException e) {
        assertEquals("The line was blank, please try again.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture constructor does throw exp on non valid train number value")
    public void TrainDepartureConstructorDoesNotThrowExceptionTrainNumber() {
      try {
        TrainDeparture trainDeparture =
            new TrainDeparture(LocalTime.of(0, 10), "F5", -5, "Bergen", 12, LocalTime.of(0, 5));
        fail("The test failed, because the constructor did not throw the exception");
      } catch (IllegalArgumentException e) {
        assertEquals(
            "The train number was not a positive integer, please try again.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture constructor does throw exp on non valid destination value")
    public void TrainDepartureConstructorDoesNotThrowExceptionDestination() {
      try {
        TrainDeparture trainDeparture =
            new TrainDeparture(LocalTime.of(0, 10), "F5", 12, "", 12, LocalTime.of(0, 5));
        fail("The test failed, because the constructor did not throw the exception");
      } catch (IllegalArgumentException e) {
        assertEquals("The destination was blank, please try again.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture constructor does throw exp on non valid track value")
    public void TrainDepartureConstructorDoesNotThrowExceptionTrack() {
      try {
        TrainDeparture trainDeparture =
            new TrainDeparture(LocalTime.of(0, 10), "F5", 12, "Bergen", -5, LocalTime.of(0, 5));
        fail("The test failed, because the constructor did not throw the exception");
      } catch (IllegalArgumentException e) {
        assertEquals("The track was not positive integer, please try again.", e.getMessage());
      }
    }

    @Test
    @DisplayName("setTrack does throw exp on not valid values")
    void setTrackThrowException() {
      TrainDeparture trainDeparture =
          new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
      try {
        trainDeparture.setTrack(-5);
        fail("The test failed, because the setter did not throw the exception");
      } catch (IllegalArgumentException e) {
        assertEquals("The track is a positive integer, please try again.", e.getMessage());
      }
    }
  }

  @Test
  void getDepartureTime() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    LocalTime expResult = LocalTime.of(0, 10);
    LocalTime result = trainDeparture.getDepartureTime();
    assertEquals(expResult, result);
  }

  @Test
  void getLine() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    String expResult = "F5";
    String result = trainDeparture.getLine();
    assertEquals(expResult, result);
  }

  @Test
  void getTrainNumber() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    int expResult = 15;
    int result = trainDeparture.getTrainNumber();
    assertEquals(expResult, result);
  }

  @Test
  void getDestination() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    String expResult = "Bergen";
    String result = trainDeparture.getDestination();
    assertEquals(expResult, result);
  }

  @Test
  void getTrack() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    int expResult = 12;
    int result = trainDeparture.getTrack();
    assertEquals(expResult, result);
  }

  @Test
  void getDelay() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    LocalTime expResult = LocalTime.of(0, 5);
    LocalTime result = trainDeparture.getDelay();
    assertEquals(expResult, result);
  }

  @Test
  void setDelay() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    LocalTime expResult = LocalTime.of(0, 12);
    trainDeparture.setDelay(LocalTime.of(0, 12));
    LocalTime result = trainDeparture.getDelay();
    assertEquals(expResult, result);
  }

  @Test
  void getDepatrueTimeWithDelay() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    LocalTime expResult = LocalTime.of(0, 15);
    LocalTime result = trainDeparture.getDepatrueTimeWithDelay();
    assertEquals(expResult, result);
  }

  @Test
  void testToString() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 5));
    String expResult = "00:10           F5     15              Bergen            00:05  12";
    String result = trainDeparture.toString();
    assertEquals(expResult, result);
  }

  @Test
  void testToStringNoDelay() {
    TrainDeparture trainDeparture =
        new TrainDeparture(LocalTime.of(0, 10), "F5", 15, "Bergen", 12, LocalTime.of(0, 0));
    String expResult = "00:10           F5     15              Bergen                   12";
    String result = trainDeparture.toString();
    assertEquals(expResult, result);
  }
}
