package edu.ntnu.stud;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class TrainDepartureRegisterTest {

  @Nested
  @DisplayName("Positive tests for the TrainDepartureRegister class")
  public class PositiveTrainDepartureTest {

    @Test
    public void testAddTrainDeparturePos1() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      boolean expResult = true;
      boolean result =
          trainDepartureRegister.addTrainDeparture(
              LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      assertEquals(expResult, result);
    }

    @Test
    public void testAddTrainDeparturePos2() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      boolean expResult = true;
      TrainDeparture train1 =
          new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      boolean result = trainDepartureRegister.addTrainDeparture(train1);
      assertEquals(expResult, result);
    }

    @Test
    public void testGetTrainDepartureTrainNumberPos() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      TrainDeparture train1 =
          new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      TrainDeparture expResult = train1;
      trainDepartureRegister.addTrainDeparture(train1);
      TrainDeparture result = trainDepartureRegister.getTrainDepartureTrainNumber(14);
      assertEquals(expResult, result);
    }

    @Test
    public void testGetTrainDepartureDestinationPos() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      TrainDeparture train1 =
          new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      ArrayList<TrainDeparture> expResult = new ArrayList<>();
      expResult.add(train1);
      trainDepartureRegister.addTrainDeparture(train1);
      ArrayList<TrainDeparture> result =
          trainDepartureRegister.getTrainDeparturesDestination("Bergen");
      assertEquals(expResult, result);
    }
  }

  @Nested
  @DisplayName("Negative tests for the TrainDepartureRegister class")
  public class NegativeTrainDepartureTest {
    @Test
    public void testAddTrainDepartureNeg1() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      boolean expResult = false;
      TrainDeparture train1 =
          new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(train1);
      boolean result =
          trainDepartureRegister.addTrainDeparture(
              LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      assertEquals(expResult, result);
    }

    @Test
    public void testAddTrainDepartureNegs2() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      boolean expResult = false;
      TrainDeparture train1 =
          new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      TrainDeparture train2 =
          new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(train1);
      boolean result = trainDepartureRegister.addTrainDeparture(train2);
      assertEquals(expResult, result);
    }

    @Test
    public void testGetTrainDepartureTrainNumberNeg() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      TrainDeparture train1 =
          new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      TrainDeparture expResult = null;
      trainDepartureRegister.addTrainDeparture(train1);
      TrainDeparture result = trainDepartureRegister.getTrainDepartureTrainNumber(15);
      assertEquals(expResult, result);
    }

    @Test
    public void testGetTrainDepartureDestinationNeg() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      TrainDeparture train1 =
          new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
      ArrayList<TrainDeparture> expResult = null;

      trainDepartureRegister.addTrainDeparture(train1);
      ArrayList<TrainDeparture> result =
          trainDepartureRegister.getTrainDeparturesDestination("Hergen");
      assertEquals(expResult, result);
    }
  }

  @Test
  void removeTrainDeparturesBeforeTime() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    TrainDeparture train1 =
        new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 12));
    TrainDeparture train2 =
        new TrainDeparture(LocalTime.of(7, 12), "L4", 15, "Bergen", 12, LocalTime.of(0, 12));

    ArrayList<TrainDeparture> expResult = new ArrayList<>();
    expResult.add(train2);
    trainDepartureRegister.addTrainDeparture(train1);
    trainDepartureRegister.addTrainDeparture(train2);
    trainDepartureRegister.removeTrainDeparturesBeforeTime(LocalTime.of(6, 20));
    ArrayList<TrainDeparture> result =
        trainDepartureRegister.getTrainDeparturesDestination("Bergen");
    assertEquals(expResult, result);
  }

  @Test
  void sortTrainDeparturesDepartureTime() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    TrainDeparture train1 =
        new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 12));
    TrainDeparture train2 =
        new TrainDeparture(LocalTime.of(3, 12), "L4", 15, "Bergen", 12, LocalTime.of(0, 12));
    ArrayList<TrainDeparture> expResult = new ArrayList<>();
    expResult.add(train2);
    expResult.add(train1);
    trainDepartureRegister.addTrainDeparture(train1);
    trainDepartureRegister.addTrainDeparture(train2);
    ArrayList<TrainDeparture> result = trainDepartureRegister.sortTrainDeparturesDepartureTime();
    assertEquals(expResult, result);
  }

  @Test
  void testToString() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    TrainDeparture train1 =
        new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 12));
    TrainDeparture train2 =
        new TrainDeparture(LocalTime.of(3, 12), "L4", 15, "Bergen", 12, LocalTime.of(0, 12));
    String expResult =
        """
    Departure Time  Line   Train Number    Destination       Delay  Track
    05:12           L4     14              Bergen            00:12  12
    03:12           L4     15              Bergen            00:12  12
    """;
    trainDepartureRegister.addTrainDeparture(train1);
    trainDepartureRegister.addTrainDeparture(train2);
    String result = trainDepartureRegister.toString();
  }
}
