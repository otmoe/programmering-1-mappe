package edu.ntnu.stud;

/** This is the main class for the train dispatch application. */
public class TrainDispatchApp {

  /**
   * Main method that runs the whole program.
   *
   * @param args String[]
   */
  public static void main(String[] args) {
    UserInterface ui = new UserInterface();
    ui.init();
    // ui.test();
    ui.start();
  }
}
