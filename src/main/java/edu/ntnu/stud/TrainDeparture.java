package edu.ntnu.stud;

import java.time.LocalTime;

/** Class containing a train departure. */
public class TrainDeparture {
  private LocalTime departureTime;
  private String line;
  private int trainNumber; // Unique number per day
  private String destination;
  private int track = -1; // Default value
  private LocalTime delay = LocalTime.of(0, 0); // Default value

  /**
   * Constructor for TrainDeparture.
   *
   * @param departureTime LocalTime is used For line String is used since has to be able to contain
   *     * both integers and characters. Does not need setter, if changed use delay.
   * @param line String is used since a combination of integers and characters is needed. Doesn't
   *     need setter line doesn't change.
   * @param trainNumber int is used because trainNumber is a "small" positive number (approximately
   *     max 1000). Doesn't need setter train number doesn't change.
   * @param destination String is used since it need to contain a word. Doesn't need setter
   *     destination doesn't change.
   * @param track int is used since it only * needs to be a small positive number (approximately max
   *     20). Needs setter track can change.
   * @param delay Localtime is used since we use LocalTime for departure time. Needs setter delay
   *     can change.
   */
  public TrainDeparture(
      LocalTime departureTime,
      String line,
      int trainNumber,
      String destination,
      int track,
      LocalTime delay) {
    if (line.equals("")) {
      throw new IllegalArgumentException("The line was blank, please try again.");
    }
    if (trainNumber <= 0) {
      throw new IllegalArgumentException(
          "The train number was not a positive integer, please try again.");
    }
    if (destination.equals("")) {
      throw new IllegalArgumentException("The destination was blank, please try again.");
    }
    if (track < -2 || track == 0) {
      throw new IllegalArgumentException("The track was not positive integer, please try again.");
    }
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = delay;
  }

  /**
   * Get method for departure time.
   *
   * @return Returns departure time
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Get method for line.
   *
   * @return Returns line
   */
  public String getLine() {
    return line;
  }

  /**
   * Get method for train number.
   *
   * @return Returns train number
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Get method for destination.
   *
   * @return Returns destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Get method for track.
   *
   * @return Retruns track
   */
  public int getTrack() {
    return track;
  }

  /**
   * Set method for track.
   *
   * @param track int
   */
  public void setTrack(int track) {
    if (track <= -2 || track == 0) {
      throw new IllegalArgumentException("The track is a positive integer, please try again.");
    }
    this.track = track;
  }

  /**
   * Get method for Delay.
   *
   * @return Returns delay
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Set method for delay.
   *
   * @param delay LocalTime
   */
  public void setDelay(LocalTime delay) {
    this.delay = delay;
  }

  /**
   * Method for getting departure time with delay.
   *
   * @return Returns departure time with delay
   */
  public LocalTime getDepatrueTimeWithDelay() {
    LocalTime depatrueTimeWithDelay = this.departureTime;
    long hours = this.delay.getHour();
    long minutes = this.delay.getMinute();
    depatrueTimeWithDelay = depatrueTimeWithDelay.plusHours(hours);
    depatrueTimeWithDelay = depatrueTimeWithDelay.plusMinutes(minutes);
    return depatrueTimeWithDelay;
  }

  /**
   * ToString method for the TrainDeparture class.
   *
   * @return Retruns String of the object variables
   */
  @Override
  public String toString() {
    String text = "";
    text += departureTime;
    for (int i = 0; i < 16 - departureTime.toString().length(); i++) {
      text += " ";
    }
    text += line;
    for (int i = 0; i < 7 - line.length(); i++) {
      text += " ";
    }
    text += trainNumber;
    for (int i = 0; i < 16 - Integer.toString(trainNumber).length(); i++) {
      text += " ";
    }
    text += destination;
    for (int i = 0; i < 18 - destination.length(); i++) {
      text += " ";
    }
    if (!delay.toString().equals("00:00")) {
      text += delay;
      for (int i = 0; i < 7 - delay.toString().length(); i++) {
        text += " ";
      }
    } else {
      for (int i = 0; i < 7; i++) {
        text += " ";
      }
    }
    if (track != -1) {
      text += track;
    }
    return text;
  }
}
