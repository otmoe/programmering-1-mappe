package edu.ntnu.stud;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Scanner;

/** Class containing User Interface. */
public class UserInterface {
  private TrainDepartureRegister trainDepartureRegister;
  private LocalTime clock;

  /** Init method that initializes the TrainDepartureRegister and program clock. */
  public void init() {
    trainDepartureRegister = new TrainDepartureRegister();
    clock = LocalTime.of(0, 0);
  }

  /** Start method that is the runs the menu and is the main UI. */
  public void start() {
    final int PRINT_TRAIN_DEPARTURES = 1;
    final int ADD_NEW_TRAIN_DEPATRTURE = 2;
    final int ASIGN_TRACK_TRAIN_DEPATRTURE = 3;
    final int ASIGN_DELAY_TRAIN_DEPATRTURE = 4;
    final int SEARCH_TRAIN_DEPATRUE_TRAIN_NUMBER = 5;
    final int SEARCH_TRAIN_DEPARTURE_DESTINATION = 6;
    final int UPDATE_CLOCK = 7;
    final int EXIT = 0;

    String input;
    LocalTime departureTime;
    String line;
    int trainNumber;
    String destination;
    int track;
    LocalTime delay;
    LocalTime newClock;

    boolean running = true;

    Scanner sc = new Scanner(System.in);
    while (running) {
      int choice = this.showMenu();
      switch (choice) {
        case PRINT_TRAIN_DEPARTURES:
          printTrainDepartures(trainDepartureRegister.sortTrainDeparturesDepartureTime());
          break;
        case ADD_NEW_TRAIN_DEPATRTURE:
          System.out.println("Enter departure time(format hh:mm):");
          input = sc.nextLine();
          try {
            departureTime = LocalTime.parse(input);
          } catch (DateTimeParseException e) {
            System.out.println("Error: Invalid time format.");
            continue;
          }
          System.out.println("Enter line:");
          line = sc.nextLine();
          System.out.println("Enter train number:");
          input = sc.nextLine();
          try {
            trainNumber = Integer.parseInt(input);
          } catch (NumberFormatException e) {
            System.out.println("Error: Invalid input.");
            continue;
          }
          System.out.println("Enter destination:");
          destination = sc.nextLine();
          System.out.println("Enter delay (format hh:mm | empty for no delay):");
          input = sc.nextLine();
          if (input == "") {
            input = "00:00";
          }
          try {
            delay = LocalTime.parse(input);
          } catch (DateTimeParseException e) {
            System.out.println("Error: Invalid time format.");
            continue;
          }
          System.out.println("Enter track (empty for no track):");
          input = sc.nextLine();
          if (input == "") {
            input = "-1";
          }
          try {
            track = Integer.parseInt(input);
          } catch (NumberFormatException e) {
            System.out.println("Error: Invalid input.");
            continue;
          }
          boolean added =
              trainDepartureRegister.addTrainDeparture(
                  departureTime, line, trainNumber, destination, track, delay);
          if (added == false) {
            System.out.println("Error: Train departure not added. Train number already exists.");
          }
          break;
        case ASIGN_TRACK_TRAIN_DEPATRTURE:
          System.out.println("Enter train number:");
          input = sc.nextLine();
          try {
            trainNumber = Integer.parseInt(input);
          } catch (NumberFormatException e) {
            System.out.println("Error: Invalid input.");
            continue;
          }
          if (trainDepartureRegister.getTrainDepartureTrainNumber(trainNumber) == null) {
            System.out.println("Error: Train not found");
            break;
          }
          System.out.println("New track: ");
          input = sc.nextLine();
          try {
            track = Integer.parseInt(input);
          } catch (NumberFormatException e) {
            System.out.println("Error: Invalid input.");
            continue;
          }
          trainDepartureRegister.getTrainDepartureTrainNumber(trainNumber).setTrack(track);
          break;
        case ASIGN_DELAY_TRAIN_DEPATRTURE:
          System.out.println("Enter train number:");
          input = sc.nextLine();
          try {
            trainNumber = Integer.parseInt(input);
          } catch (NumberFormatException e) {
            System.out.println("Error: Invalid input.");
            continue;
          }
          if (trainDepartureRegister.getTrainDepartureTrainNumber(trainNumber) == null) {
            System.out.println("Error: Train not found");
            break;
          }
          System.out.println("New delay (hh:mm):");
          input = sc.nextLine();
          try {
            delay = LocalTime.parse(input);
          } catch (DateTimeParseException e) {
            System.out.println("Error: Invalid time format.");
            continue;
          }
          trainDepartureRegister.getTrainDepartureTrainNumber(trainNumber).setDelay(delay);
          break;
        case SEARCH_TRAIN_DEPATRUE_TRAIN_NUMBER:
          System.out.println("Enter train number:");
          input = sc.nextLine();
          try {
            trainNumber = Integer.parseInt(input);
          } catch (NumberFormatException e) {
            System.out.println("Error: Invalid input.");
            continue;
          }
          if (trainDepartureRegister.getTrainDepartureTrainNumber(trainNumber) == null) {
            System.out.println("Error: Train not found");
            break;
          }
          printTrainDepartures(trainDepartureRegister.getTrainDepartureTrainNumber(trainNumber));
          break;
        case SEARCH_TRAIN_DEPARTURE_DESTINATION:
          System.out.println("Enter destination:");
          destination = sc.nextLine();
          if (trainDepartureRegister.getTrainDeparturesDestination(destination) == null) {
            System.out.println(
                "Error: No trains with " + destination + " as destination was found");
            break;
          }
          printTrainDepartures(trainDepartureRegister.getTrainDeparturesDestination(destination));
          break;
        case UPDATE_CLOCK:
          System.out.println("Enter new time (hh:mm):");
          input = sc.nextLine();
          try {
            newClock = LocalTime.parse(input);
          } catch (DateTimeParseException e) {
            System.out.println("Error: Invalid time format.");
            continue;
          }
          if (newClock.isAfter(clock)) {
            clock = newClock;
            trainDepartureRegister.removeTrainDeparturesBeforeTime(clock);
          } else {
            System.out.println("Error: new time is less then previous time");
          }
          break;
        case EXIT:
          running = false;
          System.out.println("Shutting down.....");
          break;
        default:
          System.out.println("Invalid menu choice");
      }
    }
  }

  /**
   * Menu method shown show to user so user can choose a menu option.
   *
   * @return Returns an int of the menu choice the user made.
   */
  public int showMenu() {
    Scanner scanner = new Scanner(System.in);
    int choice = -1;
    System.out.println(
        """
            Menu
            **************************************************
            1: Print train departures
            2: Add new train departure
            3: Assign track to a train departure
            4: Assign delay to a train departure
            5: Search for a train depatrue using train number
            6: Search for train depatures using destination
            7: Update the clock
            0: Exit
            **************************************************
            """);
    if (scanner.hasNextInt()) {
      choice = scanner.nextInt();
    } else {
      System.out.println("You must enter a number, not text");
    }
    return choice;
  }

  /**
   * Method for printing a ArrayList of train departures to console.
   *
   * @param trainDepartures ArrayList<TrainDeparture>
   */
  public void printTrainDepartures(ArrayList<TrainDeparture> trainDepartures) {
    String text = "Departure Time  Line   Train Number    Destination       Delay  Track\n";
    for (TrainDeparture trainDeparture : trainDepartures) {
      text += trainDeparture.toString() + "\n";
    }
    System.out.println(text);
  }

  /**
   * Method for printing a TrainDeparture to console.
   *
   * @param trainDeparture TrainDeparture
   */
  public void printTrainDepartures(TrainDeparture trainDeparture) {
    String text = "Departure Time  Line   Train Number    Destination       Delay  Track\n";
    text += trainDeparture.toString() + "\n";
    System.out.println(text);
  }

  /** Test method to test UserInterface, TrainDeparture and TrainDepartureRegister */
  public void test() {
    TrainDeparture train1 =
        new TrainDeparture(LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
    TrainDeparture train2 =
        new TrainDeparture(LocalTime.of(7, 22), "L2", 18, "Bergen", 15, LocalTime.of(0, 4));
    TrainDeparture train3 =
        new TrainDeparture(LocalTime.of(3, 2), "R1", 60, "Bergen", 12, LocalTime.of(1, 0));
    TrainDeparture train4 =
        new TrainDeparture(LocalTime.of(9, 5), "R2", 70, "Oslo", 15, LocalTime.of(0, 0));

    System.out.println(train1);

    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(5, 12), "L4", 14, "Bergen", 12, LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(7, 22), "L2", 18, "Bergen", 15, LocalTime.of(0, 4));
    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(3, 2), "R1", 60, "Bergen", 12, LocalTime.of(1, 0));
    trainDepartureRegister.addTrainDeparture(train4);
  }
}
