package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;

/** Class containing a register of train departures. */
public class TrainDepartureRegister {
  private ArrayList<TrainDeparture> trainDepartures;

  /**
   * Constructor for TrainDepartureRegister using ArrayList because its simple, easy to use, and
   * it's what im most familiar with.
   */
  public TrainDepartureRegister() {
    trainDepartures = new ArrayList<>();
  }

  /**
   * Method for adding a new TrainDeparture given TrainDeparture params. Checks if the train number
   * already exists in the register.
   *
   * @see TrainDeparture
   * @param departureTime LocalTime
   * @param line String
   * @param trainNumber int
   * @param destination String
   * @param track int
   * @param delay LocalTime
   * @return Returns true if the train departure where added, false if it didn't get added.
   */
  public boolean addTrainDeparture(
      LocalTime departureTime,
      String line,
      int trainNumber,
      String destination,
      int track,
      LocalTime delay) {
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getTrainNumber() == trainNumber) {
        return false;
      }
    }
    TrainDeparture newTrainDeparture =
        new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);
    trainDepartures.add(newTrainDeparture);
    return true;
  }

  /**
   * Method for adding a new TrainDeparture given a trainDeparture. Checks if the train number
   * already exists in the register.
   *
   * @param trainDeparture TrainDeparture
   * @return Returns true if the train departure where added, false if it didn't get added.
   */
  public boolean addTrainDeparture(TrainDeparture trainDeparture) {
    for (TrainDeparture trainDepartureCheck : trainDepartures) {
      if (trainDepartureCheck.getTrainNumber() == trainDeparture.getTrainNumber()) {
        return false;
      }
    }
    trainDepartures.add(trainDeparture);
    return true;
  }

  /**
   * Method for finding a Train Departure given its train number.
   *
   * @param trainNumber int
   * @return Returns the train departure with the given train number or null if it doesn't exist.
   */
  public TrainDeparture getTrainDepartureTrainNumber(int trainNumber) {
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getTrainNumber() == trainNumber) {
        return trainDeparture;
      }
    }
    return null;
  }

  /**
   * Method for getting a list of train departures given the destination.
   *
   * @param destination String
   * @return Returns a ArrayList<TrainDeparture> with train departures with the given destination or
   *     null if none where found.
   */
  public ArrayList<TrainDeparture> getTrainDeparturesDestination(String destination) {
    ArrayList<TrainDeparture> trainDeparturesDestination = new ArrayList<>();
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getDestination().equals(destination)) {
        trainDeparturesDestination.add(trainDeparture);
      }
    }
    if (trainDeparturesDestination.isEmpty()) {
      return null;
    }
    return trainDeparturesDestination;
  }

  /**
   * Method for removing train departures before a given time. Includes the delay of the train
   * departure.
   *
   * @param time LocalTime
   */
  public void removeTrainDeparturesBeforeTime(LocalTime time) {
    Iterator<TrainDeparture> i = trainDepartures.iterator();
    while (i.hasNext()) {
      TrainDeparture trainDeparture = i.next();
      if (trainDeparture.getDepatrueTimeWithDelay().isBefore(time)) {
        i.remove();
      }
    }
  }

  /**
   * Method for sorting the train departures by time.
   *
   * @return Returns a copy of the train departure register sorted by time.
   */
  public ArrayList<TrainDeparture> sortTrainDeparturesDepartureTime() {
    ArrayList<TrainDeparture> trainDeparturesCopy =
        (ArrayList<TrainDeparture>) trainDepartures.clone();
    trainDeparturesCopy.trimToSize();
    int count = 0;
    for (TrainDeparture trainDeparture : trainDeparturesCopy) {
      TrainDeparture soFarLowest = trainDeparture;
      for (TrainDeparture trainDepartureCheck : trainDeparturesCopy) {
        if (soFarLowest.getDepartureTime().compareTo(trainDepartureCheck.getDepartureTime()) > 0
            && trainDeparturesCopy.indexOf(trainDepartureCheck) >= count) {
          soFarLowest = trainDepartureCheck;
        }
      }
      TrainDeparture tmp = soFarLowest;
      int indexOfSoFarLowest = trainDeparturesCopy.indexOf(soFarLowest);
      int indexOfTrainDeparture = trainDeparturesCopy.indexOf(trainDeparture);
      trainDeparturesCopy.set(indexOfSoFarLowest, trainDeparture);
      trainDeparturesCopy.set(indexOfTrainDeparture, tmp);
      count++;
    }
    return trainDeparturesCopy;
  }

  /**
   * ToString method for the TrainDepartureRegister class.
   *
   * @return Returns all the train departures as String
   */
  @Override
  public String toString() {
    String text = "Departure Time  Line   Train Number    Destination       Delay  Track\n";
    for (TrainDeparture trainDeparture : trainDepartures) {
      text += trainDeparture.toString() + "\n";
    }
    return text;
  }
}
