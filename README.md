# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

 
STUDENT ID = "10049"

## Project description

TrainDispatchApp  
This is an app to track Train Departures from a single Station

[//]: # (TODO: Write a short description of your project/product here.)

## Project structure

Main Package includes:  
TrainDepatrue class  
TrainDepatrueRegister class  
TrainDispatchApp class  
UserInterface class  

Test Package includes:  
TrainDepatrueTest class  
TrainDepatrueRegisterTest class  
UserInterfaceTest class  

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)

## Link to repository

https://gitlab.stud.idi.ntnu.no/otmoe/programmering-1-mappe/-/tree/master

[//]: # (TODO: Include a link to your repository here.)

## How to run the project

Run the TranDispatchApp class whitch is the main class that runs the UI.
The main method runs the init and start of the UI and the UI runs the rest.
Inputs are integers in a menu in the terminal then in the menu options you have other types of inputs that get u different outputs in the terminal.

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)

## References

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
